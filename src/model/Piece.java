package model;

import java.awt.Point;
import java.util.ArrayList;

public abstract class Piece {

	public static final int CHESSBOARD_ROW = 8;
	public static final int CHESSBOARD_COL = 8;

	private Team team;

	public static enum Team {
		UP_TEAM, DOWN_TEAM;
	}

	public Piece(Team team) {
		this.team = team;
	}

	public abstract ArrayList<Point> getMoves(int x, int y);

	public boolean pieceIsOnMyTeam(Piece piece) {
		return this.team == piece.getTeam();
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
}
